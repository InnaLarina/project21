﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    public partial class Form1 : Form
    {
        string filepath;
        string initialDirectory= @"D:\C#Education\Git\Fasade\HomeWork\HomeWork\Materials\";
        public Form1()
        {
            InitializeComponent();
            UpDownHoriz.Maximum = this.Size.Width;
            UpDownVert.Maximum = this.Size.Height;
        }

        private void bCreateMotivator_Click(object sender, EventArgs e)
        {
            ControlImage ci = new ControlImage(pB);
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = initialDirectory;
                openFileDialog.Filter = "txt files (*.bmp)|*.txt|(*.jpg)|*.jpg";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filepath = openFileDialog.FileName;
                    ci.CreateImage(openFileDialog.FileName);
                }
            }
            SizeImage si = new SizeImage(pB);
            si.ResizeImage((int)UpDownHoriz.Value, (int)UpDownVert.Value);
            TextOnImage ti = new TextOnImage(pB);
            ti.WriteTextOnImage(tB.Text);
            FrameImage fi = new FrameImage(pB);
            fi.FrameOnImage();
        }

        private void bSaveToFile_Click(object sender, EventArgs e)
        {

            PictureToFile pf = new PictureToFile(pB);
            pf.PictureIntoFile(@"D:\C#Education\Git\Fasade\HomeWork\HomeWork\Files\motivator.bmp");

        }

        private void bResize_Click(object sender, EventArgs e)
        {
            SizeImage si = new SizeImage(pB);
            si.ResizeImage((int)UpDownHoriz.Value, (int)UpDownVert.Value);
        }

        private void bSign_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(filepath))
            {
                ControlImage ci = new ControlImage(pB);
                ci.CreateImage(filepath);
                SizeImage si = new SizeImage(pB);
                si.ResizeImage((int)UpDownHoriz.Value, (int)UpDownVert.Value);
                TextOnImage ti = new TextOnImage(pB);
                ti.WriteTextOnImage(tB.Text);
                FrameImage fi = new FrameImage(pB);
                fi.FrameOnImage();
            }
            else
                MessageBox.Show("Нет картинки! Нажмите на кнопку <Создать мотиватор>");
        }
    }
}
