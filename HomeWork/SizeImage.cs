﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class SizeImage
    {
        public PictureBox PBox { get; }
        public SizeImage(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void ResizeImage(int xSize, int ySize)
        {
          PBox.ClientSize = new Size(xSize, ySize);
            if (PBox.Image != null)
            {
                Bitmap bmp = new Bitmap(PBox.Image, PBox.ClientSize);
                PBox.Image = (Image)bmp;
            }
        }
    }
}
