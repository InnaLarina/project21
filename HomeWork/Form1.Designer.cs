﻿namespace HomeWork
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bCreateMotivator = new System.Windows.Forms.Button();
            this.pB = new System.Windows.Forms.PictureBox();
            this.UpDownHoriz = new System.Windows.Forms.NumericUpDown();
            this.UpDownVert = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bSaveToFile = new System.Windows.Forms.Button();
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.bResize = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tB = new System.Windows.Forms.TextBox();
            this.bSign = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpDownHoriz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpDownVert)).BeginInit();
            this.SuspendLayout();
            // 
            // bCreateMotivator
            // 
            this.bCreateMotivator.Location = new System.Drawing.Point(430, 415);
            this.bCreateMotivator.Name = "bCreateMotivator";
            this.bCreateMotivator.Size = new System.Drawing.Size(123, 23);
            this.bCreateMotivator.TabIndex = 0;
            this.bCreateMotivator.Text = "Создать мотиватор";
            this.bCreateMotivator.UseVisualStyleBackColor = true;
            this.bCreateMotivator.Click += new System.EventHandler(this.bCreateMotivator_Click);
            // 
            // pB
            // 
            this.pB.Location = new System.Drawing.Point(88, 76);
            this.pB.Name = "pB";
            this.pB.Size = new System.Drawing.Size(100, 50);
            this.pB.TabIndex = 1;
            this.pB.TabStop = false;
            // 
            // UpDownHoriz
            // 
            this.UpDownHoriz.Location = new System.Drawing.Point(147, 502);
            this.UpDownHoriz.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.UpDownHoriz.Name = "UpDownHoriz";
            this.UpDownHoriz.Size = new System.Drawing.Size(54, 20);
            this.UpDownHoriz.TabIndex = 2;
            this.UpDownHoriz.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            // 
            // UpDownVert
            // 
            this.UpDownVert.Location = new System.Drawing.Point(370, 502);
            this.UpDownVert.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.UpDownVert.Name = "UpDownVert";
            this.UpDownVert.Size = new System.Drawing.Size(52, 20);
            this.UpDownVert.TabIndex = 3;
            this.UpDownVert.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 502);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Размер по горизонтали";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(234, 502);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Размер по вертикали";
            // 
            // bSaveToFile
            // 
            this.bSaveToFile.Location = new System.Drawing.Point(430, 502);
            this.bSaveToFile.Name = "bSaveToFile";
            this.bSaveToFile.Size = new System.Drawing.Size(123, 23);
            this.bSaveToFile.TabIndex = 6;
            this.bSaveToFile.Text = "Сохранить в файл";
            this.bSaveToFile.UseVisualStyleBackColor = true;
            this.bSaveToFile.Click += new System.EventHandler(this.bSaveToFile_Click);
            // 
            // fileDialog
            // 
            this.fileDialog.FileName = "openFileDialog1";
            // 
            // bResize
            // 
            this.bResize.Location = new System.Drawing.Point(430, 473);
            this.bResize.Name = "bResize";
            this.bResize.Size = new System.Drawing.Size(123, 23);
            this.bResize.TabIndex = 7;
            this.bResize.Text = "Изменить размер";
            this.bResize.UseVisualStyleBackColor = true;
            this.bResize.Click += new System.EventHandler(this.bResize_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 473);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Надпись";
            // 
            // tB
            // 
            this.tB.Location = new System.Drawing.Point(61, 470);
            this.tB.Name = "tB";
            this.tB.Size = new System.Drawing.Size(361, 20);
            this.tB.TabIndex = 9;
            this.tB.Text = "Все получится!";
            // 
            // bSign
            // 
            this.bSign.Location = new System.Drawing.Point(430, 444);
            this.bSign.Name = "bSign";
            this.bSign.Size = new System.Drawing.Size(123, 23);
            this.bSign.TabIndex = 10;
            this.bSign.Text = "Изменить надпись";
            this.bSign.UseVisualStyleBackColor = true;
            this.bSign.Click += new System.EventHandler(this.bSign_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 534);
            this.Controls.Add(this.bSign);
            this.Controls.Add(this.tB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bResize);
            this.Controls.Add(this.bSaveToFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UpDownVert);
            this.Controls.Add(this.UpDownHoriz);
            this.Controls.Add(this.pB);
            this.Controls.Add(this.bCreateMotivator);
            this.Name = "Form1";
            this.Text = "Шаблон фасад";
            ((System.ComponentModel.ISupportInitialize)(this.pB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpDownHoriz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpDownVert)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCreateMotivator;
        private System.Windows.Forms.PictureBox pB;
        private System.Windows.Forms.NumericUpDown UpDownHoriz;
        private System.Windows.Forms.NumericUpDown UpDownVert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bSaveToFile;
        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.Button bResize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tB;
        private System.Windows.Forms.Button bSign;
    }
}

