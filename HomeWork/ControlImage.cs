﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace HomeWork
{
    class ControlImage
    {
        public PictureBox PBox {get;}
        public ControlImage(PictureBox pbox) 
        {
            this.PBox = pbox;
        }
        public void CreateImage(string pathName)
        {
            Bitmap bmap;
            try
            {
                bmap = new Bitmap(pathName);
                PBox.SizeMode = PictureBoxSizeMode.Zoom;
                PBox.Image = (Image)bmap;
            }
            catch (Exception e) 
            {
                MessageBox.Show("Файл с картинкой не найден!");
            }
            
        }
    }
}
