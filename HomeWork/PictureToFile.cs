﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class PictureToFile
    {
        public PictureBox PBox { get; }
        public PictureToFile(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void PictureIntoFile(string filepath)
        {
            if (PBox.Image != null) 
            { 
                (PBox.Image as Bitmap).Save(filepath, System.Drawing.Imaging.ImageFormat.Bmp);
                MessageBox.Show("Мотиватор сохранен!");
            }
            else
                MessageBox.Show("Нет файла мотиватора! Нечего сохранять.");    
        }
    }
}
