Разработано приложение Windows Forms

В приложениb используются классы:

ControlImage: поле типа PictureBox, конструктор, метод CreateImage(string pathName), рисующий картинку на поле PictureBox

SizeImage: поле типа PictureBox, конструктор, метод ResizeImage(int xSize, int ySize), изменяющий размер картинки.

FrameImage: поле типа PictureBox, конструктор, метод FrameOnImage(), рисующий рамочку.

TextOnImage: поле типа PictureBox, конструктор, метод WriteTextOnImage(string messageToSoul), делающий надпись.

Картинки для тестирования программы находятся по пути \Materials

Код тестирования в классе Form1.